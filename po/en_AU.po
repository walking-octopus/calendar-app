# English (Australia) translation for ubuntu-calendar-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the ubuntu-calendar-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-calendar-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-04 09:07+0000\n"
"PO-Revision-Date: 2016-03-27 10:09+0000\n"
"Last-Translator: Jared Norris <jarednorris@ubuntu.com>\n"
"Language-Team: English (Australia) <en_AU@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2017-04-05 07:14+0000\n"
"X-Generator: Launchpad (build 18335)\n"

#: lomiri-calendar-app.desktop.in:7
msgid "/usr/share/lomiri-calendar-app/assets/lomiri-calendar-app@30.png"
msgstr ""

#: lomiri-calendar-app.desktop.in:8 qml/EventDetails.qml:348
#: qml/NewEvent.qml:677
msgid "Calendar"
msgstr "Calendar"

#: lomiri-calendar-app.desktop.in:9
#, fuzzy
#| msgid "A calendar for Ubuntu which syncs with online accounts."
msgid "A calendar for Lomiri which syncs with online accounts."
msgstr "A calendar for Ubuntu which syncs with online accounts."

#: lomiri-calendar-app.desktop.in:10
msgid "calendar;event;day;week;year;appointment;meeting;"
msgstr "calendar;event;day;week;year;appointment;meeting;"

#: qml/AgendaView.qml:51 qml/calendar.qml:373 qml/calendar.qml:554
msgid "Agenda"
msgstr "Agenda"

#: qml/AgendaView.qml:101
msgid "You have no calendars enabled"
msgstr "You have no calendars enabled"

#: qml/AgendaView.qml:101
msgid "No upcoming events"
msgstr "No upcoming events"

#: qml/AgendaView.qml:113
msgid "Enable calendars"
msgstr "Enable calendars"

#: qml/AgendaView.qml:206
msgid "no event name set"
msgstr "no event name set"

#: qml/AgendaView.qml:208
msgid "no location"
msgstr "no location"

#: qml/AllDayEventComponent.qml:89 qml/TimeLineBase.qml:50
msgid "New event"
msgstr "New event"

#. TRANSLATORS: Please keep the translation of this string to a max of
#. 5 characters as the week view where it is shown has limited space.
#: qml/AllDayEventComponent.qml:148
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1 event"
msgstr[1] "%1 events"

#. TRANSLATORS: the argument refers to the number of all day events
#: qml/AllDayEventComponent.qml:152
msgid "%1 all day event"
msgid_plural "%1 all day events"
msgstr[0] "%1 all day event"
msgstr[1] "%1 all day events"

#: qml/CalendarChoicePopup.qml:45 qml/EventActions.qml:55
msgid "Calendars"
msgstr "Calendars"

#: qml/CalendarChoicePopup.qml:47 qml/SettingsPage.qml:51
msgid "Back"
msgstr "Back"

#. TRANSLATORS: Please translate this string  to 15 characters only.
#. Currently ,there is no way we can increase width of action menu currently.
#: qml/CalendarChoicePopup.qml:59 qml/EventActions.qml:40
msgid "Sync"
msgstr "Sync"

#: qml/CalendarChoicePopup.qml:59 qml/EventActions.qml:40
msgid "Syncing"
msgstr "Syncing"

#: qml/CalendarChoicePopup.qml:84
msgid "Add online Calendar"
msgstr "Add online Calendar"

#: qml/CalendarChoicePopup.qml:189
msgid "Unable to deselect"
msgstr "Unable to deselect"

#: qml/CalendarChoicePopup.qml:190
msgid ""
"In order to create new events you must have at least one writable calendar "
"selected"
msgstr ""
"In order to create new events you must have at least one writable calendar "
"selected"

#: qml/CalendarChoicePopup.qml:192 qml/CalendarChoicePopup.qml:205
msgid "Ok"
msgstr "Ok"

#: qml/CalendarChoicePopup.qml:202
msgid "Network required"
msgstr ""

#: qml/CalendarChoicePopup.qml:203
msgid ""
"You are currently offline. In order to add online accounts you must have "
"network connection available."
msgstr ""

#: qml/ColorPickerDialog.qml:25
msgid "Select Color"
msgstr "Select Colour"

#: qml/ColorPickerDialog.qml:55 qml/DeleteConfirmationDialog.qml:63
#: qml/EditEventConfirmationDialog.qml:52 qml/NewEvent.qml:394
#: qml/OnlineAccountsHelper.qml:73 qml/RemindersPage.qml:88
msgid "Cancel"
msgstr "Cancel"

#: qml/ContactChoicePopup.qml:37
msgid "No contact"
msgstr "No contact"

#: qml/ContactChoicePopup.qml:96
msgid "Search contact"
msgstr "Search contact"

#: qml/DayView.qml:76 qml/MonthView.qml:51 qml/WeekView.qml:60
#: qml/YearView.qml:57
msgid "Today"
msgstr "Today"

#. TRANSLATORS: this is a time formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#. It's used in the header of the month and week views
#: qml/DayView.qml:129 qml/MonthView.qml:84 qml/WeekView.qml:159
msgid "MMMM yyyy"
msgstr "MMMM yyyy"

#: qml/DeleteConfirmationDialog.qml:31
msgid "Delete Recurring Event"
msgstr "Delete Recurring Event"

#: qml/DeleteConfirmationDialog.qml:32
msgid "Delete Event"
msgstr "Delete Event"

#. TRANSLATORS: argument (%1) refers to an event name.
#: qml/DeleteConfirmationDialog.qml:36
msgid "Delete only this event \"%1\", or all events in the series?"
msgstr "Delete only this event \"%1\", or all events in the series?"

#: qml/DeleteConfirmationDialog.qml:37
msgid "Are you sure you want to delete the event \"%1\"?"
msgstr "Are you sure you want to delete the event \"%1\"?"

#: qml/DeleteConfirmationDialog.qml:40
msgid "Delete series"
msgstr "Delete series"

#: qml/DeleteConfirmationDialog.qml:51
msgid "Delete this"
msgstr "Delete this"

#: qml/DeleteConfirmationDialog.qml:51 qml/NewEvent.qml:401
msgid "Delete"
msgstr "Delete"

#: qml/EditEventConfirmationDialog.qml:29 qml/NewEvent.qml:389
msgid "Edit Event"
msgstr "Edit Event"

#. TRANSLATORS: argument (%1) refers to an event name.
#: qml/EditEventConfirmationDialog.qml:32
msgid "Edit only this event \"%1\", or all events in the series?"
msgstr "Edit only this event \"%1\", or all events in the series?"

#: qml/EditEventConfirmationDialog.qml:35
msgid "Edit series"
msgstr "Edit series"

#: qml/EditEventConfirmationDialog.qml:44
msgid "Edit this"
msgstr "Edit this"

#: qml/EventActions.qml:67 qml/SettingsPage.qml:49
msgid "Settings"
msgstr "Settings"

#. TRANSLATORS: the first argument (%1) refers to a start time for an event,
#. while the second one (%2) refers to the end time
#: qml/EventBubble.qml:139
msgid "%1 - %2"
msgstr "%1 - %2"

#: qml/EventDetails.qml:37 qml/NewEvent.qml:596
msgid "Event Details"
msgstr "Event Details"

#: qml/EventDetails.qml:40
msgid "Edit"
msgstr "Edit"

#: qml/EventDetails.qml:173 qml/TimeLineHeader.qml:66 qml/calendar.qml:750
msgid "All Day"
msgstr "All Day"

#: qml/EventDetails.qml:392
msgid "Attending"
msgstr "Attending"

#: qml/EventDetails.qml:394
msgid "Not Attending"
msgstr "Not Attending"

#: qml/EventDetails.qml:396
msgid "Maybe"
msgstr ""

#: qml/EventDetails.qml:398
msgid "No Reply"
msgstr "No Reply"

#: qml/EventDetails.qml:437 qml/NewEvent.qml:632
msgid "Description"
msgstr "Description"

#: qml/EventDetails.qml:464 qml/NewEvent.qml:867 qml/NewEvent.qml:884
msgid "Reminder"
msgstr "Reminder"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the page to choose repetition
#. and as the header of the list item that shows the repetition
#. summary in the page that displays the event details
#: qml/EventRepetition.qml:40 qml/EventRepetition.qml:179
msgid "Repeat"
msgstr "Repeat"

#: qml/EventRepetition.qml:199
msgid "Repeats On:"
msgstr "Repeats On:"

#: qml/EventRepetition.qml:245
#, fuzzy
msgid "Interval of recurrence"
msgstr "After X Occurrence"

#: qml/EventRepetition.qml:270
msgid "Recurring event ends"
msgstr "Recurring event ends"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the option selector to choose
#. its repetition
#: qml/EventRepetition.qml:294 qml/NewEvent.qml:840
msgid "Repeats"
msgstr "Repeats"

#: qml/EventRepetition.qml:320 qml/NewEvent.qml:494
msgid "Date"
msgstr "Date"

#. TRANSLATORS: the argument refers to multiple recurrence of event with count .
#. E.g. "Daily; 5 times."
#: qml/EventUtils.qml:75
msgid "%1; %2 time"
msgid_plural "%1; %2 times"
msgstr[0] "%1; %2 time"
msgstr[1] "%1; %2 times"

#. TRANSLATORS: the argument refers to recurrence until user selected date.
#. E.g. "Daily; until 12/12/2014."
#: qml/EventUtils.qml:79
msgid "%1; until %2"
msgstr "%1; until %2"

#: qml/EventUtils.qml:93
msgid "; every %1 days"
msgstr ""

#: qml/EventUtils.qml:95
msgid "; every %1 weeks"
msgstr ""

#: qml/EventUtils.qml:97
msgid "; every %1 months"
msgstr ""

#: qml/EventUtils.qml:99
msgid "; every %1 years"
msgstr ""

#. TRANSLATORS: the argument refers to several different days of the week.
#. E.g. "Weekly on Mondays, Tuesdays"
#: qml/EventUtils.qml:125
msgid "Weekly on %1"
msgstr "Weekly on %1"

#: qml/LimitLabelModel.qml:25
msgid "Never"
msgstr "Never"

#: qml/LimitLabelModel.qml:26
msgid "After X Occurrence"
msgstr "After X Occurrence"

#: qml/LimitLabelModel.qml:27
msgid "After Date"
msgstr "After Date"

#. TRANSLATORS: This is shown in the month view as "Wk" as a title
#. to indicate the week numbers. It should be a max of up to 3 characters.
#: qml/MonthComponent.qml:294
msgid "Wk"
msgstr "Wk"

#: qml/MonthView.qml:79 qml/WeekView.qml:140
msgid "%1 %2"
msgstr "%1 %2"

#: qml/NewEvent.qml:206
msgid "End time can't be before start time"
msgstr "End time can't be before start time"

#: qml/NewEvent.qml:389 qml/NewEventBottomEdge.qml:54
msgid "New Event"
msgstr "New Event"

#: qml/NewEvent.qml:419
msgid "Save"
msgstr "Save"

#: qml/NewEvent.qml:430
msgid "Error"
msgstr "Error"

#: qml/NewEvent.qml:432
msgid "OK"
msgstr "OK"

#: qml/NewEvent.qml:494
msgid "From"
msgstr "From"

#: qml/NewEvent.qml:519
msgid "To"
msgstr "To"

#: qml/NewEvent.qml:548
msgid "All day event"
msgstr "All day event"

#: qml/NewEvent.qml:585
msgid "Event Name"
msgstr "Event Name"

#: qml/NewEvent.qml:605
#, fuzzy
msgid "More details"
msgstr "Event Details"

#: qml/NewEvent.qml:656
msgid "Location"
msgstr "Location"

#: qml/NewEvent.qml:744
msgid "Guests"
msgstr "Guests"

#: qml/NewEvent.qml:754
msgid "Add Guest"
msgstr "Add Guest"

#: qml/OnlineAccountsHelper.qml:39
msgid "Pick an account to create."
msgstr ""

#: qml/RecurrenceLabelDefines.qml:23
msgid "Once"
msgstr "Once"

#: qml/RecurrenceLabelDefines.qml:24
msgid "Daily"
msgstr "Daily"

#: qml/RecurrenceLabelDefines.qml:25
msgid "On Weekdays"
msgstr "On Weekdays"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday, Tuesday, Thursday"
#: qml/RecurrenceLabelDefines.qml:27
msgid "On %1, %2 ,%3"
msgstr "On %1, %2 ,%3"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday and Thursday"
#: qml/RecurrenceLabelDefines.qml:29
msgid "On %1 and %2"
msgstr "On %1 and %2"

#: qml/RecurrenceLabelDefines.qml:30
msgid "Weekly"
msgstr "Weekly"

#: qml/RecurrenceLabelDefines.qml:31
msgid "Monthly"
msgstr "Monthly"

#: qml/RecurrenceLabelDefines.qml:32
msgid "Yearly"
msgstr "Yearly"

#: qml/RemindersModel.qml:31 qml/RemindersModel.qml:99
msgid "No Reminder"
msgstr "No Reminder"

#. TRANSLATORS: this refers to when a reminder should be shown as a notification
#. in the indicators. "On Event" means that it will be shown right at the time
#. the event starts, not any time before
#: qml/RemindersModel.qml:34 qml/RemindersModel.qml:103
msgid "On Event"
msgstr "On Event"

#: qml/RemindersModel.qml:43 qml/RemindersModel.qml:112
msgid "%1 week"
msgid_plural "%1 weeks"
msgstr[0] ""
msgstr[1] ""

#: qml/RemindersModel.qml:54 qml/RemindersModel.qml:110
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] ""
msgstr[1] ""

#: qml/RemindersModel.qml:65 qml/RemindersModel.qml:108
#: qml/SettingsPage.qml:274 qml/SettingsPage.qml:275 qml/SettingsPage.qml:276
#: qml/SettingsPage.qml:277
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] ""
msgstr[1] ""

#: qml/RemindersModel.qml:74 qml/SettingsPage.qml:270 qml/SettingsPage.qml:271
#: qml/SettingsPage.qml:272 qml/SettingsPage.qml:273
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] ""
msgstr[1] ""

#: qml/RemindersModel.qml:104 qml/RemindersModel.qml:105
#: qml/RemindersModel.qml:106 qml/RemindersModel.qml:107
#: qml/SettingsPage.qml:385 qml/SettingsPage.qml:386 qml/SettingsPage.qml:387
#: qml/SettingsPage.qml:388 qml/SettingsPage.qml:389 qml/SettingsPage.qml:390
#: qml/SettingsPage.qml:391 qml/SettingsPage.qml:392
#, fuzzy
msgid "%1 minutes"
msgstr "15 minutes"

#: qml/RemindersModel.qml:109
#, fuzzy
msgid "%1 hours"
msgstr "1 hour"

#: qml/RemindersModel.qml:111
#, fuzzy
msgid "%1 days"
msgstr "1 day"

#: qml/RemindersModel.qml:113
#, fuzzy
msgid "%1 weeks"
msgstr "1 week"

#: qml/RemindersModel.qml:114
msgid "Custom"
msgstr ""

#: qml/RemindersPage.qml:62
msgid "Custom reminder"
msgstr ""

#: qml/RemindersPage.qml:73
#, fuzzy
msgid "Set reminder"
msgstr "Default reminder"

#: qml/SettingsPage.qml:85
msgid "Show week numbers"
msgstr "Show week numbers"

#: qml/SettingsPage.qml:104
msgid "Display Chinese calendar"
msgstr ""

#: qml/SettingsPage.qml:125
msgid "Business hours"
msgstr ""

#: qml/SettingsPage.qml:232
msgid "Data refresh interval"
msgstr ""

#: qml/SettingsPage.qml:262
msgid ""
"Note: Automatic syncronisation currently only works while the app is open in "
"the foreground. Alternatively set background suspension to off. Then sync "
"will work while the app is open in the background."
msgstr ""

#: qml/SettingsPage.qml:316
msgid "Default reminder"
msgstr "Default reminder"

#: qml/SettingsPage.qml:363
msgid "Default length of new event"
msgstr ""

#: qml/SettingsPage.qml:426
msgid "Default calendar"
msgstr "Default calendar"

#: qml/SettingsPage.qml:495
msgid "Theme"
msgstr ""

#: qml/SettingsPage.qml:518
msgid "System theme"
msgstr ""

#: qml/SettingsPage.qml:519
msgid "SuruDark theme"
msgstr ""

#: qml/SettingsPage.qml:520
msgid "Ambiance theme"
msgstr ""

#. TRANSLATORS: W refers to Week, followed by the actual week number (%1)
#: qml/TimeLineHeader.qml:54
msgid "W%1"
msgstr "W%1"

#: qml/WeekView.qml:147 qml/WeekView.qml:148
msgid "MMM"
msgstr "MMM"

#: qml/YearView.qml:83
msgid "Year %1"
msgstr "Year %1"

#: qml/calendar.qml:99
msgid ""
"Calendar app accept four arguments: --starttime, --endtime, --newevent and --"
"eventid. They will be managed by system. See the source for a full comment "
"about them"
msgstr ""
"Calendar app accept four arguments: --starttime, --endtime, --newevent and --"
"eventid. They will be managed by system. See the source for a full comment "
"about them"

#: qml/calendar.qml:381 qml/calendar.qml:575
msgid "Day"
msgstr "Day"

#: qml/calendar.qml:389 qml/calendar.qml:596
msgid "Week"
msgstr "Week"

#: qml/calendar.qml:397 qml/calendar.qml:617
msgid "Month"
msgstr "Month"

#: qml/calendar.qml:405 qml/calendar.qml:638
msgid "Year"
msgstr "Year"

#~ msgid "5 minutes"
#~ msgstr "5 minutes"

#~ msgid "30 minutes"
#~ msgstr "30 minutes"

#~ msgid "2 hours"
#~ msgstr "2 hours"

#~ msgid "2 days"
#~ msgstr "2 days"

#~ msgid "2 weeks"
#~ msgstr "2 weeks"

#~ msgid "Show lunar calendar"
#~ msgstr "Show lunar calendar"
