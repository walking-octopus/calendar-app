# Polish translation for ubuntu-calendar-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the ubuntu-calendar-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-calendar-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-04 09:07+0000\n"
"PO-Revision-Date: 2023-01-25 21:52+0000\n"
"Last-Translator: gnu-ewm <gnu.ewm@protonmail.com>\n"
"Language-Team: Polish <https://hosted.weblate.org/projects/lomiri/"
"lomiri-calendar-app/pl/>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.16-dev\n"
"X-Launchpad-Export-Date: 2017-04-05 07:14+0000\n"

#: lomiri-calendar-app.desktop.in:7
msgid "/usr/share/lomiri-calendar-app/assets/lomiri-calendar-app@30.png"
msgstr "/usr/share/lomiri-calendar-app/assets/lomiri-calendar-app@30.png"

#: lomiri-calendar-app.desktop.in:8 qml/EventDetails.qml:348
#: qml/NewEvent.qml:677
msgid "Calendar"
msgstr "Kalendarz"

#: lomiri-calendar-app.desktop.in:9
msgid "A calendar for Lomiri which syncs with online accounts."
msgstr "Kalendarz Lomiri z synchronizacją kont online."

#: lomiri-calendar-app.desktop.in:10
msgid "calendar;event;day;week;year;appointment;meeting;"
msgstr "kalendarz;wydarzenie;dzień;tydzień;rok;wizyta;spotkanie;"

#: qml/AgendaView.qml:51 qml/calendar.qml:373 qml/calendar.qml:554
msgid "Agenda"
msgstr "Wydarzenia"

#: qml/AgendaView.qml:101
msgid "You have no calendars enabled"
msgstr "Brak aktywnych kalendarzy"

#: qml/AgendaView.qml:101
msgid "No upcoming events"
msgstr "Brak nadchodzących wydarzeń"

#: qml/AgendaView.qml:113
msgid "Enable calendars"
msgstr "Aktywuj kalendarz"

#: qml/AgendaView.qml:206
msgid "no event name set"
msgstr "Nie wpisano nazwy wydarzenia"

#: qml/AgendaView.qml:208
msgid "no location"
msgstr "nie ustawiono miejsca"

#: qml/AllDayEventComponent.qml:89 qml/TimeLineBase.qml:50
msgid "New event"
msgstr "Nowe wydarzenie"

#. TRANSLATORS: Please keep the translation of this string to a max of
#. 5 characters as the week view where it is shown has limited space.
#: qml/AllDayEventComponent.qml:148
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1 wydarzenie"
msgstr[1] "%1 wydarzenia"
msgstr[2] "%1 wydarzeń"

#. TRANSLATORS: the argument refers to the number of all day events
#: qml/AllDayEventComponent.qml:152
msgid "%1 all day event"
msgid_plural "%1 all day events"
msgstr[0] "%1 wydarzenie całodniowe"
msgstr[1] "%1 wydarzenia całodniowe"
msgstr[2] "%1 wydarzeń całodniowych"

#: qml/CalendarChoicePopup.qml:45 qml/EventActions.qml:55
msgid "Calendars"
msgstr "Kalendarze"

#: qml/CalendarChoicePopup.qml:47 qml/SettingsPage.qml:51
msgid "Back"
msgstr "Wstecz"

#. TRANSLATORS: Please translate this string  to 15 characters only.
#. Currently ,there is no way we can increase width of action menu currently.
#: qml/CalendarChoicePopup.qml:59 qml/EventActions.qml:40
msgid "Sync"
msgstr "Synchronizuj"

#: qml/CalendarChoicePopup.qml:59 qml/EventActions.qml:40
msgid "Syncing"
msgstr "Synchronizowanie"

#: qml/CalendarChoicePopup.qml:84
msgid "Add online Calendar"
msgstr "Dodaj kalendarz online"

#: qml/CalendarChoicePopup.qml:189
msgid "Unable to deselect"
msgstr "Nie można usunąć zaznaczenia"

#: qml/CalendarChoicePopup.qml:190
msgid ""
"In order to create new events you must have at least one writable calendar "
"selected"
msgstr ""
"Aby utworzyć nowe wydarzenia, należy wybrać przynajmniej jeden zapisywalny "
"kalendarz"

#: qml/CalendarChoicePopup.qml:192 qml/CalendarChoicePopup.qml:205
msgid "Ok"
msgstr "OK"

#: qml/CalendarChoicePopup.qml:202
msgid "Network required"
msgstr "Wymagana połączenia sieciowego"

#: qml/CalendarChoicePopup.qml:203
msgid ""
"You are currently offline. In order to add online accounts you must have "
"network connection available."
msgstr ""
"Obecnie nie masz połączenia z Internetem. Aby dodać konta online musisz mieć "
"dostępne połączenie z siecią."

#: qml/ColorPickerDialog.qml:25
msgid "Select Color"
msgstr "Wybierz kolor"

#: qml/ColorPickerDialog.qml:55 qml/DeleteConfirmationDialog.qml:63
#: qml/EditEventConfirmationDialog.qml:52 qml/NewEvent.qml:394
#: qml/OnlineAccountsHelper.qml:73 qml/RemindersPage.qml:88
msgid "Cancel"
msgstr "Anuluj"

#: qml/ContactChoicePopup.qml:37
msgid "No contact"
msgstr "Brak kontaktu"

#: qml/ContactChoicePopup.qml:96
msgid "Search contact"
msgstr "Szukaj kontaktu"

#: qml/DayView.qml:76 qml/MonthView.qml:51 qml/WeekView.qml:60
#: qml/YearView.qml:57
msgid "Today"
msgstr "Dzisiaj"

#. TRANSLATORS: this is a time formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#. It's used in the header of the month and week views
#: qml/DayView.qml:129 qml/MonthView.qml:84 qml/WeekView.qml:159
msgid "MMMM yyyy"
msgstr "MMMM yyyy"

#: qml/DeleteConfirmationDialog.qml:31
msgid "Delete Recurring Event"
msgstr "Usuń powtarzające się zdarzenie"

#: qml/DeleteConfirmationDialog.qml:32
msgid "Delete Event"
msgstr "Usuń wydarzenie"

#. TRANSLATORS: argument (%1) refers to an event name.
#: qml/DeleteConfirmationDialog.qml:36
msgid "Delete only this event \"%1\", or all events in the series?"
msgstr "Skasować tylko to jedno wydarzenie „%1”, czy całą ich serię?"

#: qml/DeleteConfirmationDialog.qml:37
msgid "Are you sure you want to delete the event \"%1\"?"
msgstr "Czy na pewno skasować wydarzenie „%1”?"

#: qml/DeleteConfirmationDialog.qml:40
msgid "Delete series"
msgstr "Usuń serię"

#: qml/DeleteConfirmationDialog.qml:51
msgid "Delete this"
msgstr "Usuń tylko to"

#: qml/DeleteConfirmationDialog.qml:51 qml/NewEvent.qml:401
msgid "Delete"
msgstr "Usuń"

#: qml/EditEventConfirmationDialog.qml:29 qml/NewEvent.qml:389
msgid "Edit Event"
msgstr "Edytuj wydarzenie"

#. TRANSLATORS: argument (%1) refers to an event name.
#: qml/EditEventConfirmationDialog.qml:32
msgid "Edit only this event \"%1\", or all events in the series?"
msgstr "Edytować tylko wydarzenie \"%1\", czy wszystkie w serii?"

#: qml/EditEventConfirmationDialog.qml:35
msgid "Edit series"
msgstr "Edytuj serię"

#: qml/EditEventConfirmationDialog.qml:44
msgid "Edit this"
msgstr "Edytuj tylko to"

#: qml/EventActions.qml:67 qml/SettingsPage.qml:49
msgid "Settings"
msgstr "Ustawienia"

#. TRANSLATORS: the first argument (%1) refers to a start time for an event,
#. while the second one (%2) refers to the end time
#: qml/EventBubble.qml:139
msgid "%1 - %2"
msgstr "%1 - %2"

#: qml/EventDetails.qml:37 qml/NewEvent.qml:596
msgid "Event Details"
msgstr "Szczegóły wydarzenia"

#: qml/EventDetails.qml:40
msgid "Edit"
msgstr "Modyfikuj"

#: qml/EventDetails.qml:173 qml/TimeLineHeader.qml:66 qml/calendar.qml:750
msgid "All Day"
msgstr "Cały dzień"

#: qml/EventDetails.qml:392
msgid "Attending"
msgstr "Uczestniczę"

#: qml/EventDetails.qml:394
msgid "Not Attending"
msgstr "Nie uczestniczę"

#: qml/EventDetails.qml:396
msgid "Maybe"
msgstr "Może"

#: qml/EventDetails.qml:398
msgid "No Reply"
msgstr "Bez powtarzania"

#: qml/EventDetails.qml:437 qml/NewEvent.qml:632
msgid "Description"
msgstr "Opis"

#: qml/EventDetails.qml:464 qml/NewEvent.qml:867 qml/NewEvent.qml:884
msgid "Reminder"
msgstr "Przypomnienie"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the page to choose repetition
#. and as the header of the list item that shows the repetition
#. summary in the page that displays the event details
#: qml/EventRepetition.qml:40 qml/EventRepetition.qml:179
msgid "Repeat"
msgstr "Powtarzanie"

#: qml/EventRepetition.qml:199
msgid "Repeats On:"
msgstr "Powtarzanie co:"

#: qml/EventRepetition.qml:245
msgid "Interval of recurrence"
msgstr "Po X wystąpieniach"

#: qml/EventRepetition.qml:270
msgid "Recurring event ends"
msgstr "Powtarzanie wydarzenia kończy się"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the option selector to choose
#. its repetition
#: qml/EventRepetition.qml:294 qml/NewEvent.qml:840
msgid "Repeats"
msgstr "Powtarzanie"

#: qml/EventRepetition.qml:320 qml/NewEvent.qml:494
msgid "Date"
msgstr "Data"

#. TRANSLATORS: the argument refers to multiple recurrence of event with count .
#. E.g. "Daily; 5 times."
#: qml/EventUtils.qml:75
msgid "%1; %2 time"
msgid_plural "%1; %2 times"
msgstr[0] "%1; %2 raz"
msgstr[1] "%1; %2 razy"
msgstr[2] "%1; %2 razy"

#. TRANSLATORS: the argument refers to recurrence until user selected date.
#. E.g. "Daily; until 12/12/2014."
#: qml/EventUtils.qml:79
msgid "%1; until %2"
msgstr "%1; do %2"

#: qml/EventUtils.qml:93
msgid "; every %1 days"
msgstr ";co %1 dni"

#: qml/EventUtils.qml:95
msgid "; every %1 weeks"
msgstr ";co %1 tygodni"

#: qml/EventUtils.qml:97
msgid "; every %1 months"
msgstr ";co %1 miesięcy"

#: qml/EventUtils.qml:99
msgid "; every %1 years"
msgstr ";co %1 lat"

#. TRANSLATORS: the argument refers to several different days of the week.
#. E.g. "Weekly on Mondays, Tuesdays"
#: qml/EventUtils.qml:125
msgid "Weekly on %1"
msgstr "Co tydzień w %1"

#: qml/LimitLabelModel.qml:25
msgid "Never"
msgstr "Nigdy"

#: qml/LimitLabelModel.qml:26
msgid "After X Occurrence"
msgstr "Po X wystąpieniach"

#: qml/LimitLabelModel.qml:27
msgid "After Date"
msgstr "Po dacie"

#. TRANSLATORS: This is shown in the month view as "Wk" as a title
#. to indicate the week numbers. It should be a max of up to 3 characters.
#: qml/MonthComponent.qml:294
msgid "Wk"
msgstr "Tydz."

#: qml/MonthView.qml:79 qml/WeekView.qml:140
msgid "%1 %2"
msgstr "%1 %2"

#: qml/NewEvent.qml:206
msgid "End time can't be before start time"
msgstr "Czas zakończenia nie może zostać ustawiony przed czasem rozpoczęcia"

#: qml/NewEvent.qml:389 qml/NewEventBottomEdge.qml:54
msgid "New Event"
msgstr "Nowe wydarzenie"

#: qml/NewEvent.qml:419
msgid "Save"
msgstr "Zapisz"

#: qml/NewEvent.qml:430
msgid "Error"
msgstr "Błąd"

#: qml/NewEvent.qml:432
msgid "OK"
msgstr "OK"

#: qml/NewEvent.qml:494
msgid "From"
msgstr "Od"

#: qml/NewEvent.qml:519
msgid "To"
msgstr "Do"

#: qml/NewEvent.qml:548
msgid "All day event"
msgstr "Wydarzenie całodniowe"

#: qml/NewEvent.qml:585
msgid "Event Name"
msgstr "Nazwa wydarzenia"

#: qml/NewEvent.qml:605
msgid "More details"
msgstr "Więcej szczegółów"

#: qml/NewEvent.qml:656
msgid "Location"
msgstr "Lokalizacja"

#: qml/NewEvent.qml:744
msgid "Guests"
msgstr "Goście"

#: qml/NewEvent.qml:754
msgid "Add Guest"
msgstr "Dodaj gościa"

#: qml/OnlineAccountsHelper.qml:39
msgid "Pick an account to create."
msgstr "Wybierz konto do utworzenia."

#: qml/RecurrenceLabelDefines.qml:23
msgid "Once"
msgstr "Raz"

#: qml/RecurrenceLabelDefines.qml:24
msgid "Daily"
msgstr "Codziennie"

#: qml/RecurrenceLabelDefines.qml:25
msgid "On Weekdays"
msgstr "W dni robocze"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday, Tuesday, Thursday"
#: qml/RecurrenceLabelDefines.qml:27
msgid "On %1, %2 ,%3"
msgstr "W %1, %2 ,%3"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday and Thursday"
#: qml/RecurrenceLabelDefines.qml:29
msgid "On %1 and %2"
msgstr "W %1 oraz %2"

#: qml/RecurrenceLabelDefines.qml:30
msgid "Weekly"
msgstr "Co tydzień"

#: qml/RecurrenceLabelDefines.qml:31
msgid "Monthly"
msgstr "Co miesiąc"

#: qml/RecurrenceLabelDefines.qml:32
msgid "Yearly"
msgstr "Co rok"

#: qml/RemindersModel.qml:31 qml/RemindersModel.qml:99
msgid "No Reminder"
msgstr "Bez przypomnienia"

#. TRANSLATORS: this refers to when a reminder should be shown as a notification
#. in the indicators. "On Event" means that it will be shown right at the time
#. the event starts, not any time before
#: qml/RemindersModel.qml:34 qml/RemindersModel.qml:103
msgid "On Event"
msgstr "Bez wyprzedzenia"

#: qml/RemindersModel.qml:43 qml/RemindersModel.qml:112
msgid "%1 week"
msgid_plural "%1 weeks"
msgstr[0] "%1 tydzień"
msgstr[1] "%1 tygodnie"
msgstr[2] "%1 tygodni"

#: qml/RemindersModel.qml:54 qml/RemindersModel.qml:110
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] "%1 dzień"
msgstr[1] "%1 dni"
msgstr[2] "%1 dni"

#: qml/RemindersModel.qml:65 qml/RemindersModel.qml:108
#: qml/SettingsPage.qml:274 qml/SettingsPage.qml:275 qml/SettingsPage.qml:276
#: qml/SettingsPage.qml:277
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1 godzinę"
msgstr[1] "%1 godziny"
msgstr[2] "%1 godzin"

#: qml/RemindersModel.qml:74 qml/SettingsPage.qml:270 qml/SettingsPage.qml:271
#: qml/SettingsPage.qml:272 qml/SettingsPage.qml:273
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minuta"
msgstr[1] "%1 minuty"
msgstr[2] "%1 minut"

#: qml/RemindersModel.qml:104 qml/RemindersModel.qml:105
#: qml/RemindersModel.qml:106 qml/RemindersModel.qml:107
#: qml/SettingsPage.qml:385 qml/SettingsPage.qml:386 qml/SettingsPage.qml:387
#: qml/SettingsPage.qml:388 qml/SettingsPage.qml:389 qml/SettingsPage.qml:390
#: qml/SettingsPage.qml:391 qml/SettingsPage.qml:392
msgid "%1 minutes"
msgstr "%1 minut"

#: qml/RemindersModel.qml:109
msgid "%1 hours"
msgstr "%1 godzin"

#: qml/RemindersModel.qml:111
msgid "%1 days"
msgstr "%1 dni"

#: qml/RemindersModel.qml:113
msgid "%1 weeks"
msgstr "%1 tygodni"

#: qml/RemindersModel.qml:114
msgid "Custom"
msgstr "Własne"

#: qml/RemindersPage.qml:62
msgid "Custom reminder"
msgstr "Własne przypomnienie"

#: qml/RemindersPage.qml:73
msgid "Set reminder"
msgstr "Ustaw przypomnienie"

#: qml/SettingsPage.qml:85
msgid "Show week numbers"
msgstr "Pokaż numery tygodni"

#: qml/SettingsPage.qml:104
msgid "Display Chinese calendar"
msgstr "Wyświetl kalendarz chiński"

#: qml/SettingsPage.qml:125
msgid "Business hours"
msgstr "Godziny pracy"

#: qml/SettingsPage.qml:232
msgid "Data refresh interval"
msgstr "Częstotliwość odświeżania danych"

#: qml/SettingsPage.qml:262
msgid ""
"Note: Automatic syncronisation currently only works while the app is open in "
"the foreground. Alternatively set background suspension to off. Then sync "
"will work while the app is open in the background."
msgstr ""

#: qml/SettingsPage.qml:316
msgid "Default reminder"
msgstr "Domyślne przypomnienie"

#: qml/SettingsPage.qml:363
msgid "Default length of new event"
msgstr "Domyślna długość nowego wydarzenia"

#: qml/SettingsPage.qml:426
msgid "Default calendar"
msgstr "Domyślny kalendarz"

#: qml/SettingsPage.qml:495
msgid "Theme"
msgstr "Motyw"

#: qml/SettingsPage.qml:518
msgid "System theme"
msgstr "Systemowy"

#: qml/SettingsPage.qml:519
msgid "SuruDark theme"
msgstr "SuruDark"

#: qml/SettingsPage.qml:520
msgid "Ambiance theme"
msgstr "Ambiance"

#. TRANSLATORS: W refers to Week, followed by the actual week number (%1)
#: qml/TimeLineHeader.qml:54
msgid "W%1"
msgstr "T%1"

#: qml/WeekView.qml:147 qml/WeekView.qml:148
msgid "MMM"
msgstr "MMM"

#: qml/YearView.qml:83
msgid "Year %1"
msgstr "Rok %1"

#: qml/calendar.qml:99
msgid ""
"Calendar app accept four arguments: --starttime, --endtime, --newevent and --"
"eventid. They will be managed by system. See the source for a full comment "
"about them"
msgstr ""
"Aplikacja kalendarza akceptuje cztery argumenty: --starttime, --endtime, --"
"newevent, --eventid. Będą one zarządzane przez system. Więcej informacji na "
"ten temat w źródłach programu"

#: qml/calendar.qml:381 qml/calendar.qml:575
msgid "Day"
msgstr "Dzień"

#: qml/calendar.qml:389 qml/calendar.qml:596
msgid "Week"
msgstr "Tydzień"

#: qml/calendar.qml:397 qml/calendar.qml:617
msgid "Month"
msgstr "Miesiąc"

#: qml/calendar.qml:405 qml/calendar.qml:638
msgid "Year"
msgstr "Rok"

#~ msgid "5 minutes"
#~ msgstr "5 minut"

#~ msgid "10 minutes"
#~ msgstr "10 minut"

#~ msgid "15 minutes"
#~ msgstr "15 minut"

#~ msgid "30 minutes"
#~ msgstr "30 minut"

#~ msgid "1 hour"
#~ msgstr "1 godzina"

#~ msgid "2 hours"
#~ msgstr "2 godziny"

#~ msgid "1 day"
#~ msgstr "1 dzień"

#~ msgid "2 days"
#~ msgstr "2 dni"

#~ msgid "1 week"
#~ msgstr "1 tydzień"

#~ msgid "2 weeks"
#~ msgstr "2 tygodnie"

#~ msgid "Show lunar calendar"
#~ msgstr "Pokaż kalendarz księżycowy"
